import React from 'react';
import {View, StyleSheet, Text} from 'react-native'
import PropTypes from 'prop-types'

export const WeatherItem = () => {
    return (
        <View style={styles.item}>
            <View>
                <Text>qweqwe</Text>
            </View>
            <View>
                <Text>fffferf</Text>
            </View>
        </View>
    );
};

WeatherItem.propTypes = {};

const styles = StyleSheet.create({
    item: {
        height: 50,
        paddingHorizontal: 20,
        marginVertical: 10,
        marginHorizontal: 10,
        backgroundColor: 'rgba(0,0,0,0.33)',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    }
})
