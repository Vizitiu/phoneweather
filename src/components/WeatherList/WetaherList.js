import React from 'react';
import {View, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import {WeatherItem} from "./WeatherItem/WeatherItem";

export const WeatherList = () => {
    return (
        <View style={styles.list}>
            <WeatherItem/>
            <WeatherItem/>
            <WeatherItem/>
            <WeatherItem/>
            <WeatherItem/>
        </View>
    );
};

WeatherList.propTypes = {

};

const styles = StyleSheet.create({
    list: {
        width: '100%'
    }
})
