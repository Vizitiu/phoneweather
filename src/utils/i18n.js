import I18n from "i18n-js";
import * as Localization from 'expo-localization'

import en from "./locales/en";
import ru from "./locales/ru";

I18n.locale = Localization.locale
I18n.fallbacks = true;

I18n.translations = {
    en,
    ru
};

export default I18n;
