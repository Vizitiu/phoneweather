import {GET_LOCATION} from "../../../constants";
import axios from 'axios'

export const getApiLocation = (lat, long) => async dispatch => {
    try {
        const res = await axios.get(`https://api.opencagedata.com/geocode/v1/json?q=${lat}+${long}&key=1d13b04292ea421ca705a10e6a494d78`)
        const country = res.data.results[0].components.country
        const town = res.data.results[0].components.town

        dispatch(getLocation(country, town))
    } catch (e) {
        console.log(e);
    }
}

export const getLocation = (country, town) => ({
    type: GET_LOCATION,
    country,
    town
})
