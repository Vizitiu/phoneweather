import {GET_WEATHER} from "../../../constants";
import axios from 'axios'

export const getApiWeather = (country, town) => async dispatch => {
    try {
        const res = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${town},${country}&appid=429736441cf3572838aa10530929f7cd`)
        const main = res.data.weather[0].main
        const desc = res.data.weather[0].desc
        const icon = res.data.weather[0].icon
        const temp = res.data.main.temp
        const tempMin = res.data.main.temp_min
        const tempMax = res.data.main.temp_max

        dispatch(getWeather(main, desc, icon, temp, tempMin, tempMax))
    } catch (e) {
        console.log(e);
    }
}

export const getWeather = (main, desc, icon, temp, tempMin, tempMax) => ({
    type: GET_WEATHER,
    main,
    desc,
    icon,
    temp,
    tempMin,
    tempMax
})
