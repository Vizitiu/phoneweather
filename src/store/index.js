import {combineReducers, createStore, applyMiddleware, compose} from "redux";
import thunk from 'redux-thunk'
import {weather} from './reducer/weather'
import {location} from "./reducer/location";

export const rootReducer = combineReducers({
    weather,
    location
})


const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

export const configStore = () => {
    const middlewares = [thunk]
    const middleWareEnhancer = applyMiddleware(...middlewares);

    const store = createStore(
        rootReducer,
        composeEnhancers(middleWareEnhancer)
    )
    return store
}

