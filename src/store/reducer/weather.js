import {GET_WEATHER} from "../../../constants";

const STATE = {
    main: '',
    desc: '',
    icon: '',
    temp: '',
    tempMin: '',
    tempMax: '',
}

export const weather = (state = STATE, {type, main, desc, icon, temp, tempMin, tempMax}) => {
    switch (type) {
        case GET_WEATHER: return {main, desc, icon, temp, tempMin, tempMax}
        default:
            return state
    }
}
