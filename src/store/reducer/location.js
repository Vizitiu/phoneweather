import {GET_LOCATION} from "../../../constants";

const STATE = {
    country: '',
    town: ''
}

export const location = (state = STATE, {type, country, town}) => {
    switch (type) {
        case GET_LOCATION:
            return {country, town}
        default:
            return state
    }
}
