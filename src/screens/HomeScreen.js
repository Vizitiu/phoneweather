import React, {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {MonoText} from '../components/ui/StyledText'
import I18n from '../utils/i18n'
import Colors from "../../constants/Colors";
import {WeatherList} from '../components/WeatherList/WetaherList'
import {useDispatch, useSelector} from "react-redux";
import {getApiLocation} from "../store/actions/location";
import {getApiWeather} from "../store/actions/weather";


export default function HomeScreen() {
    const dispatch = useDispatch()

    // Weather
    const main = useSelector(({weather}) => weather.main)
    const temp = useSelector(({weather}) => weather.temp) - 273.15

    // Location
    const country = useSelector(({location}) => location.country)
    const town = useSelector(({location}) => location.town)

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(position => {
            const lat = parseFloat(position.coords.latitude)
            const long = parseFloat(position.coords.longitude)
            dispatch(getApiLocation(lat, long))
        })
    },);

    useEffect(() => {
        dispatch(getApiWeather(country, town))
    },)

    return (
        <View style={styles.container}>
            <MonoText style={styles.dataTitle}>Tuesday, 27th August</MonoText>
            <MonoText style={styles.degreesTitle}>{main}, {temp.toFixed()}c</MonoText>
            <MonoText style={styles.cityTitle}>{country}, {town}</MonoText>
            <WeatherList/>
        </View>
    );
}

HomeScreen.navigationOptions = {header: null}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.backgroundColorOne,
    },
    dataTitle: {
        color: '#fff',
        fontSize: 18
    },
    degreesTitle: {
        color: '#fff',
        fontSize: 70,
        marginTop: 25,
    },
    cityTitle: {
        color: '#fff',
        fontSize: 14,
        marginBottom: 30
    }
})
