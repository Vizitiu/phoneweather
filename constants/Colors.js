export default {
    backgroundColorOne: 'rgba(26,26,26,0.73)',
    backgroundColorTwo: '#91d6d5',
    errorColor: '#ff002a',
    successColor: '#35e600',
};
